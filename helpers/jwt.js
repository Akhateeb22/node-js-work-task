const createHttpError = require('http-errors');
const JWT = require('jsonwebtoken');
require('dotenv').config();


function signAccessToken(userId, next) {
    return new Promise((resolve, reject) => {
        const paylod = {}
        const secret = process.env.ACCESS_TOKEN_SECRET;
        const options = {
            audience: `${userId}`
        }
        JWT.sign(paylod, secret, options, (err, token) => {
            if (err) next(err)
            resolve(token);
        });
    })
}

function verifyAccessToken(req, res, next) {
    try {
        if (!req.headers['authorization'])
            throw createHttpError.Unauthorized('No JWT found')
        const token = req.headers['authorization'];
        JWT.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
            if (err) {
                const message = err.name === 'JsonWebTokenError' ? 'Unauthorized' : err.message
                throw createHttpError.Unauthorized(message);
            }
            req.payload = payload;
            next();
        });
    } catch (error) {
        next(error);
    }

}

exports.verifyAccessToken = verifyAccessToken;
exports.signAccessToken = signAccessToken;