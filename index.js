const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const users = require('./Routes/users');


app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())

app.use('/users', users);

app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.send({
        error: {
            status: err.status || 500,
            message: err.message,
        },
    })
});


app.listen(4000, () => {
    console.log('Listening on port 4000')
});