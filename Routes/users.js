const express = require('express');
const fs = require('fs')
const createHttpError = require('http-errors');
const router = express.Router();
const {
    verifyAccessToken,
    signAccessToken
} = require('../helpers/jwt')

router.get('/getAllUsers', verifyAccessToken, (req, res, next) => {
    try {
        let users = fs.readFileSync('./Data/users.json', 'utf8');
        res.json(JSON.parse(users));
    } catch (error) {
        next(error);
    }

});

router.post('/addNewUser', async (req, res, next) => {
    try {
        let users = fs.readFileSync('./Data/users.json', 'utf8');
        users = JSON.parse(users);
        users.forEach(user => {
            if (user['Email'] == req.body.Email)
                throw createHttpError.Conflict(`${req.body.Email} is already registered`);

        })
        const accessToken = await signAccessToken(req.body.id, next);
        res.header('jwt_token', accessToken);
        users.push(req.body);
        fs.writeFileSync('./Data/users.json', JSON.stringify(users));
        res.send(`User ${req.body.UserName} has been added sucessfully`);
    } catch (error) {
        next(error);
    }


});


router.put('/updateUserInfo/:id', verifyAccessToken, (req, res, next) => {
    try {
        let users = fs.readFileSync('./Data/users.json', 'utf8');
        users = JSON.parse(users);
        let recordIndex = -1;
        for (let i = 0; i < users.length; i++) {
            if (users[i]["ID"] == req.params.id) {
                recordIndex = i;
                break;
            }
        }
        if (recordIndex == -1) throw createHttpError.NotFound(`User with the given ID: {${req.params.id}} not found`);
        let bodyKeys = Object.keys(req.body);
        let bodyValues = Object.values(req.body);
        for (let i = 0; i < bodyKeys.length; i++) {
            if(bodyKeys[i].localeCompare("ID")===0){
                throw createHttpError.Unauthorized("Can't edit ID");
                
            }
            users[recordIndex][bodyKeys[i]] = bodyValues[i]
        }
        fs.writeFileSync('./Data/users.json', JSON.stringify(users));
        res.send(`User with the given ID {${req.params.id}} has been updated sucessfully`);
    } catch (error) {
        next(error);
    }

});


router.delete('/deleteUser/:id', verifyAccessToken, (req, res, next) => {
    try {
        let usersData = fs.readFileSync('./Data/users.json', 'utf8');
        usersData = JSON.parse(usersData);
        let recordIndex = -1;
        for (let i = 0; i < usersData.length; i++) {
            if (usersData[i]["ID"] == req.params.id) {
                recordIndex = i;
            }
        }
        if (recordIndex == -1)
            throw new createHttpError.NotFound(`User with the given ID: {${req.params.id}} not found `)
        usersData.splice(recordIndex, 1);
        fs.writeFileSync('./Data/users.json', JSON.stringify(usersData));
        res.send(`User with the given ID: {${req.params.id}} has been deleted sucessfully`)
    } catch (error) {
        next(error);
    }

})


module.exports = router;